from setuptools import setup

with open('requirements.txt') as origin:
    requirements = origin.readlines()

setup(name='Ata',
      version='0.1',
      long_description='A Flask app',
      packages=['ata'],
      zip_safe=False,
      include_package_data=True,
      install_requires=requirements)
