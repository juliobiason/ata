#!/usr/bin/env python
# -*- encoding: utf-8 -*-

"""Simple Flask app."""

import os
import os.path

import markdown

from flask import abort
from flask import Flask
from flask import render_template

app = Flask(__name__)

app.config.from_object('ata.defaults')
app.config.from_envvar('ATA_SETTINGS', silent=True)


@app.route('/')
def index():
    content = os.listdir(app.config['STORAGE'])
    data = []
    for entry in sorted(content, reverse=True):
        app.logger.debug('Found %s', entry)
        if not entry.endswith('.md'):
            continue

        with open(os.path.join(app.config['STORAGE'], entry)) as origin:
            content = origin.read()
            output = markdown.markdown(content)
            try:
                first_paragraph_end = output.index('</p>')
                paragraph = output[:first_paragraph_end]
            except ValuError:
                paragraph = output

            entry_name = entry[:entry.find('.md')]
            data.append((entry_name, output))

    return render_template('index.html', data=data)

@app.route('/<entry_name>')
def show_entry(entry_name):
    filename = os.path.join(app.config['STORAGE'], entry_name + '.md')
    with open(filename) as origin:
        content = origin.read()
        output = markdown.markdown(content)

    return render_template('entry.html', output=output,
                           entry=entry_name)


@app.errorhandler(404)
def page_not_found():
    return render_template('page_not_found.html')


@app.errorhandler(FileNotFoundError)
def entry_not_found(_):
    return render_template('entry_not_found.html')
